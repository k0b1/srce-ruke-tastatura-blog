import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { FrontpageComponent } from "./components/frontpage/frontpage.component";
import { PageNotFoundComponent } from "./components/page-not-found/page-not-found.component";
import { TagsComponent } from "./components/tags/tags.component";
import { PretragaComponent } from "./components/pretraga/pretraga.component";
import { SinglePageComponent } from "./components/shared/single-page/single-page.component";
import { SearchBySectionComponent } from "./components/pretraga/search-by-section/search-by-section.component";
import { PremotavanjeModule } from "./components/premotavanje/premotavanje.module";
import { BlogModule } from "./components/blog/blog.module";
import { BbModule } from "./components/bb/bb.module";

const routes: Routes = [
  {
    path: "",
    component: FrontpageComponent,
    pathMatch: "full"
  },
  {
    path: "premotavanje",
    loadChildren: () =>
      import("./components/premotavanje/premotavanje.module").then(
        m => m.PremotavanjeModule
      )
  },
  {
    path: "blog",
    loadChildren: () =>
      import("./components/blog/blog.module").then(m => m.BlogModule)
  },
  {
    path: "b1t0v11bajt0v1",
    loadChildren: () =>
      import("./components/bb/bb.module").then(m => m.BbModule)
  },
  {
    path: "log",
    component: SinglePageComponent,
    pathMatch: "full"
  },
  {
    path: ":post_type/tagovi/:tag_name/:tag_id/:pageID",
    component: SearchBySectionComponent,
    pathMatch: "full"
  },
  {
    path: "tags/:tag_name/:tag_id",
    component: TagsComponent,
    pathMatch: "full"
  },
  {
    path: "pretraga/:searchItem",
    component: PretragaComponent,
    pathMatch: "full"
  },
  {
    path: ":post_type/pretraga/:searchItem/:pageID",
    component: SearchBySectionComponent,
    pathMatch: "full"
  },
  {
    path: "it-is-the-wrong-em-boyo",
    component: PageNotFoundComponent,
    pathMatch: "full"
  },
  {
    path: "**",
    redirectTo: "/it-is-the-wrong-em-boyo"
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    PremotavanjeModule,
    BlogModule,
    BbModule
  ],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}
