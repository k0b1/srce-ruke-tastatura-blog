import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";

import { Observable } from "rxjs";
//import 'rxjs/Rx';
import { map } from "rxjs/operators";

import { environment } from "../../environments/environment";
// import { Post } from "./post";

import { Post } from "./post.model";

@Injectable({
  providedIn: "root"
})

/*
!!!!! BETTER ORGANIZE FUNCTIONS BELOW, THEY BECAME SPAGHETI
*/
export class PostsService {
  private postsUrl = environment.baseUrl + "/index.php/wp-json/wp/v2/";
  private categoriesUrl =
    environment.baseUrl + "index.php/wp-json/wp/v2/categories";

  constructor(private httpClient: HttpClient) {}

  /* frontpage http request */
  getIntroPostByTag(tagID: number) {
    return this.httpClient.get<[]>(this.postsUrl + "posts?tags=" + tagID);
  }
  /* create get requests for different post types */
  mainGetRequest(
    post_type: string = "",
    tagID = "",
    numberOfPosts = "12",
    page_no = "1",
    category
  ): Observable<Post[]> {
    switch (post_type) {
      case "posts":
        return this.httpClient.get<Post[]>(this.postsUrl + post_type);
        break;
      case "b1t0v11bajt0v1":
        return this.httpClient.get<Post[]>(this.postsUrl + post_type, {
          params: {
            per_page: numberOfPosts,
            type: post_type,
            _embed: "",
            order: "asc"
          }
        });
        break;
      case "premotavanje":
        return this.httpClient.get<Post[]>(
          this.postsUrl + post_type + "?_embed",
          {
            params: {
              kategorije: tagID,
              per_page: numberOfPosts,
              page: page_no
            }
          }
        );
        break;
      case "blog":
        return this.httpClient
          .get<Post[]>(
            this.postsUrl +
              post_type +
              "?page=" +
              page_no +
              "&type=" +
              post_type,
            {
              params: {
                per_page: numberOfPosts
              }
            }
          )
          .pipe(
            map(response => {
              let resultArray: Post[] = [];
              for (let post in response) {
                let resultObj = Object.create(null);
                for (let postProp in response[post]) {
                  // test
                  if (
                    postProp === "date" ||
                    postProp === "content" ||
                    postProp === "excerpt" ||
                    postProp === "author" ||
                    postProp === "tags" ||
                    postProp === "number_of_posts" ||
                    postProp === "featured_image_src" ||
                    postProp === "rendered" ||
                    postProp === "title" ||
                    postProp === "featured_media" ||
                    postProp === "tags_name" ||
                    postProp === "slug" ||
                    postProp === "type"
                  ) {
                    resultObj[postProp] = response[post][postProp];
                  }
                }
                resultArray.push(resultObj);
              }
              return resultArray;
            })
          );
        break;
      case "premotavanje/tagovi":
        return this.httpClient.get<Post[]>(this.postsUrl + "premotavanje", {
          params: {
            tags: tagID,
            per_page: numberOfPosts,
            page: page_no ? page_no : "0", //??????
            type: post_type
          }
        });
        break;
      case "premotavanje/knjige":
        return this.httpClient.get<Post[]>(this.postsUrl + "premotavanje", {
          params: {
            kategorije: tagID,
            per_page: numberOfPosts,
            page: page_no ? page_no : "0" //??????
          }
        });
        break;
      case "premotavanje/muzika":
        return this.httpClient.get<Post[]>(this.postsUrl + "premotavanje", {
          params: {
            kategorije: tagID,
            per_page: numberOfPosts,
            page: page_no ? page_no : "0", //??????
            type: "premotavanje"
          }
        });
        break;
      case "b1t0v11bajt0v1/tagovi":
        return this.httpClient.get<Post[]>(this.postsUrl + "b1t0v11bajt0v1", {
          params: {
            tags: tagID,
            per_page: numberOfPosts,
            page: page_no ? page_no : "0",
            type: post_type
          }
        });
        break;
      case "blog/tagovi":
        return this.httpClient.get<Post[]>(this.postsUrl + "blog", {
          params: {
            tags: tagID,
            per_page: numberOfPosts,
            page: page_no ? page_no : "0",
            type: post_type
          }
        });
        break;
      case "premotavanje/pretraga":
        return this.httpClient.get<Post[]>(this.postsUrl + "premotavanje", {
          params: {
            search: tagID,
            per_page: numberOfPosts,
            type: "search_premotavanje"
          }
        });
        break;
      case "blog/pretraga":
        return this.httpClient.get<Post[]>(this.postsUrl + "blog", {
          params: {
            search: tagID,
            per_page: numberOfPosts,
            page: page_no ? page_no : "1",
            type: "search_blog"
          }
        });
        break;
      case "b1t0v11bajt0v1/pretraga":
        return this.httpClient.get<Post[]>(this.postsUrl + "b1t0v11bajt0v1", {
          params: {
            search: tagID,
            per_page: numberOfPosts,
            type: "search_bb"
          }
        });
        break;
      case "":
        break;
      //do nothing
    }
  }
  /*  */
  listTags(
    cpt: string,
    tagID: string,
    numberOfPosts: number
  ): Observable<any[]> {
    return this.httpClient.get<any[]>(this.postsUrl + cpt, {
      params: {
        tags: tagID
      }
    });
  }

  /* send request for individual post, 'bb', 'premotavanje', 'blog'*/
  getPost(cpt, post_slug): Observable<any[]> {
    return this.httpClient.get<any[]>(this.postsUrl + cpt, {
      params: {
        slug: post_slug
      }
    });
  }

  getCategory(): Observable<any[]> {
    return this.httpClient.get<any[]>(this.categoriesUrl);
  }

  getPostsByCategory(cpt, cat_id): Observable<any[]> {
    return this.httpClient.get<any[]>(this.postsUrl + cpt, {
      params: {
        categories: cat_id
      }
    });
  }

  getSearchInput(cpt, searchItem): Observable<any[]> {
    return this.httpClient.get<any[]>(this.postsUrl + cpt, {
      params: {
        search: searchItem
      }
    });
  }

  //get citation from database
  getCitation() {
    return this.httpClient.get<any[]>(this.postsUrl + "citati");
  }
}
