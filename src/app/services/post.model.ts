export interface Post {
  date: string;
  slug: string;
  type: string;
  title: {};
  content: {};
  excerpt: {};
  author: Number;
  featured_media: Number;
  tags: [];
  featured_img_src: string;
  number_of_posts: string;
  tags_name: [{}];
}
