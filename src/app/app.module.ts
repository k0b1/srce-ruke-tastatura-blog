import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HeaderComponent } from "./layouts/header/header.component";
import { BodyComponent } from "./layouts/body/body.component";
import { FooterComponent } from "./layouts/footer/footer.component";
import { FrontpageComponent } from "./components/frontpage/frontpage.component";
import { AuthenticationComponent } from "./authentication/authentication.component";
import { PageNotFoundComponent } from "./components/page-not-found/page-not-found.component";
import { TagsComponent } from "./components/tags/tags.component";
import { SidebarComponent } from "./layouts/sidebar/sidebar.component";
import { PretragaComponent } from "./components/pretraga/pretraga.component";
import { PostsService } from "./services/posts.service";
import { SinglePageComponent } from "./components/shared/single-page/single-page.component";
import { BbFrontpageComponent } from "./components/frontpage/bb-frontpage/bb-frontpage.component";
import { PremotavanjeFrontpageComponent } from "./components/frontpage/premotavanje-frontpage/premotavanje-frontpage.component";
import { BlogFrontpageComponent } from "./components/frontpage/blog-frontpage/blog-frontpage.component";
import { FrontpageColumnComponent } from "./components/frontpage/frontpage-column/frontpage-column.component";
import { FrontpageRowsComponent } from "./components/frontpage/frontpage-rows/frontpage-rows.component";
import { ListTagsFrontpageComponent } from "./components/shared/list-tags-frontpage/list-tags.component";
import { ListPostDataFrontpageComponent } from "./components/shared/list-post-data-frontpage/list-post-data.component";
import { PremotavanjeFrontpageTabsComponent } from "./components/frontpage/premotavanje-frontpage/premotavanje-frontpage-tabs/premotavanje-frontpage-tabs.component";
import { SearchBySectionComponent } from "./components/pretraga/search-by-section/search-by-section.component";
import { IsLoadingModule } from "./components/shared/is-loading/is-loading.module";
import { SinglePostModule } from "./components/shared/list-posts-row/single-post.module";
import { BlogModule } from "./components/blog/blog.module";
import { BbModule } from "./components/bb/bb.module";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    FrontpageComponent,
    AuthenticationComponent,
    PageNotFoundComponent,
    TagsComponent,
    SidebarComponent,
    PretragaComponent,
    SinglePageComponent,
    BbFrontpageComponent,
    PremotavanjeFrontpageComponent,
    BlogFrontpageComponent,
    FrontpageColumnComponent,
    FrontpageRowsComponent,
    ListTagsFrontpageComponent,
    ListPostDataFrontpageComponent,
    PremotavanjeFrontpageTabsComponent,
    SearchBySectionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    SinglePostModule,
    IsLoadingModule,
    BlogModule,
    BbModule
  ],
  providers: [PostsService],
  bootstrap: [AppComponent]
})
export class AppModule {}
