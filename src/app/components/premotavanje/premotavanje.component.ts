import { Component, OnInit } from '@angular/core';
import { PostsService } from '../../services/posts.service';

@Component({
  selector: 'app-premotavanje',
  templateUrl: './premotavanje.component.html',
  styleUrls: ['./premotavanje.component.scss']
})
export class PremotavanjeComponent implements OnInit {
  review: any[];
  /* Review section variables*/
  music: any[];
  hide: boolean = false;
  opacity: string = '';
  selectedMusicPost : string = "";
  reviewImage: string = "";
  reviewTitle: string = "";
  reviewID: string = "";
  reviewExcerpt : string = "";
  /* end of Review section variables */
  
  constructor( private postsService : PostsService ) { }

  ngOnInit() {
  	this.getReviewPosts('premotavanje', 12);
  }

  getReviewPosts(post_type, numberOfPosts) {
    
    this.postsService
      .mainGetRequest(post_type, "190, 191", numberOfPosts, '1',"")
      .subscribe( result => {
        this.review = result;
        this.music = result;
      })
  }

  onHoverShowMusicData(post) {
      /* data to be sent to the right div container*/
    this.reviewImage = post.featured_image_src;
    this.reviewTitle = post.title.rendered;
    this.reviewID = post.id;
    this.reviewExcerpt = post.excerpt.rendered;
  }

  onLeaveMusicImage() {
    this.selectedMusicPost = "";
  }

}
