import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { SidebarComponent } from '../../../layouts/sidebar/sidebar.component';
import { PostsService } from '../../../services/posts.service';

@Component({
  selector: 'app-knjige',
  templateUrl: './knjige.component.html',
  styleUrls: ['./knjige.component.scss']
})
export class KnjigeComponent implements OnInit {
  post: any[];
  isFrontPage: boolean = false;
    total_number_of_posts: string; 
  currentPage: number = 1;

  id: number;
  constructor( private postsService: PostsService, private route: ActivatedRoute) { }

  ngOnInit() {
    
    // this.getCategory();
    this.getBookPosts('premotavanje/knjige', 190, 10, 1, ""); //190 => knjige category id
  }

  getBookPosts(post_type, tagID, numberOfPosts, page_no, category) {
    this.postsService.mainGetRequest(post_type, tagID, numberOfPosts, page_no, category)
      .subscribe( result => {
        this.total_number_of_posts = result[0].number_of_posts;
        this.post = result;  
      })
  }
 

}
