import { NgModule } from "@angular/core";
import { PremotavanjeComponent } from "./premotavanje.component";
import { MuzikaComponent } from "./muzika/muzika.component";
import { KnjigeComponent } from "./knjige/knjige.component";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { SinglePostModule } from "../shared/list-posts-row/single-post.module";
import { SinglePageComponent } from "../shared/single-page/single-page.component";

const routes: Routes = [
  {
    path: "premotavanje",
    component: PremotavanjeComponent,
    pathMatch: "full"
  },
  {
    path: "premotavanje/muzika",
    component: MuzikaComponent,
    pathMatch: "full"
  },
  {
    path: "premotavanje/muzika/page/:pageID",
    component: MuzikaComponent,
    pathMatch: "full"
  },
  {
    path: "premotavanje/muzika/:post_slug",
    component: SinglePageComponent,
    pathMatch: "full"
  },
  {
    path: "premotavanje/knjige",
    component: KnjigeComponent,
    pathMatch: "full"
  },
  {
    path: "premotavanje/knjige/:post_slug",
    component: SinglePageComponent,
    pathMatch: "full"
  },
  {
    path: "premotavanje/:post_slug",
    component: SinglePageComponent,
    pathMatch: "full"
  }
];

@NgModule({
  declarations: [PremotavanjeComponent, MuzikaComponent, KnjigeComponent],
  imports: [RouterModule.forChild(routes), CommonModule, SinglePostModule],
  exports: [
    PremotavanjeComponent,
    MuzikaComponent,
    KnjigeComponent,
    RouterModule
  ]
})
export class PremotavanjeModule {}
