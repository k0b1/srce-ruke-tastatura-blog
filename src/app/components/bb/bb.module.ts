import { BbComponent } from "./bb.component";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { IsLoadingModule } from "../shared/is-loading/is-loading.module";
import { SinglePostModule } from "../shared/list-posts-row/single-post.module";
import { RouterModule, Routes } from "@angular/router";
import { SinglePageComponent } from "../shared/single-page/single-page.component";

const routes: Routes = [
  {
    path: "b1t0v11bajt0v1",
    component: BbComponent,
    pathMatch: "full"
  },
  {
    path: "b1t0v11bajt0v1/:post_slug",
    component: SinglePageComponent,
    pathMatch: "full"
  }
];

@NgModule({
  declarations: [BbComponent],
  imports: [
    CommonModule,
    IsLoadingModule,
    SinglePostModule,
    RouterModule.forChild(routes)
  ],
  exports: [BbComponent]
})
export class BbModule {}
