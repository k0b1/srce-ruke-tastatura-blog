import { Component, OnInit } from "@angular/core";

// import { SidebarComponent } from "../../layouts/sidebar/sidebar.component";
import { PostsService } from "../../services/posts.service";
import { Router } from "@angular/router";
import { ListPostsColumnComponent } from "../shared/list-posts-column/list-posts-column.component";

@Component({
  selector: "app-bb",
  templateUrl: "./bb.component.html",
  styleUrls: ["./bb.component.scss"]
})
export class BbComponent implements OnInit {
  bb: any[];
  isFrontPage: boolean = false;
  isLoading: boolean = true;
  constructor(private postsService: PostsService, private router: Router) {}

  ngOnInit() {
    this.getB1BPosts("b1t0v11bajt0v1", "3");
  }

  getB1BPosts(post_type: string, numberOfPosts: string) {
    this.postsService
      .mainGetRequest(post_type, "10", numberOfPosts, "0", 1)
      .subscribe(result => {
        this.bb = result;
        this.isLoading = false;
      });
  }
}
