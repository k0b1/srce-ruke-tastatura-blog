import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";

import { PaginationComponent } from "../shared/pagination/pagination.component";
import { SidebarComponent } from "../../layouts/sidebar/sidebar.component";
import { ListPostsColumnComponent } from "../shared/list-posts-column/list-posts-column.component";
import { PostsService } from "../../services/posts.service";

// import { PaginationService } from '../../shared/services/pagination.service';

@Component({
  selector: "app-blog",
  templateUrl: "./blog.component.html",
  styleUrls: ["./blog.component.scss"],
})
export class BlogComponent implements OnInit {
  blog: any[];
  total_number_of_posts: string;
  currentPage: number = 1;
  post_type: string = "blog";
  isFrontPage: boolean;
  isLoading: boolean = true;

  constructor(
    private postsService: PostsService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    if (this.router.url == "/") {
      this.isFrontPage = true;
      this.getBlogPosts("blog", "", 3, this.currentPage, "");
      //      this.isLoading = false;
    } else if (this.router.url == "/blog") {
      /*pagination blog load, listen to changes in url*/
      this.route.params.subscribe((params: Params) => {
        if (params["pageID"]) this.currentPage = params["pageID"];
        this.getBlogPosts("blog", "", 10, this.currentPage, "");
        window.scrollTo(0, 0);
      });
    } else {
      this.route.params.subscribe((params: Params) => {
        this.isLoading = true;
        if (params["pageID"]) this.currentPage = params["pageID"];
        this.getBlogPosts("blog", "", 10, this.currentPage, "");
        window.scrollTo(0, 0);
      });
    }
  }

  getBlogPosts(post_type, tagID, numberOfPosts, page_no, category) {
    this.postsService
      .mainGetRequest(post_type, tagID, numberOfPosts, page_no, category)
      .subscribe((result) => {
        this.total_number_of_posts = result[0].number_of_posts;
        this.blog = result;
        this.isLoading = false;
      });
  }
}
