import { NgModule } from "@angular/core";
import { BlogComponent } from "./blog.component";
import { IsLoadingModule } from "../shared/is-loading/is-loading.module";
import { SinglePostModule } from "../shared/list-posts-row/single-post.module";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { SinglePageComponent } from "../shared/single-page/single-page.component";

const routes: Routes = [
  {
    path: "blog",
    component: BlogComponent,
    pathMatch: "full"
  },
  {
    path: "blog/:post_slug",
    component: SinglePageComponent,
    pathMatch: "full"
  },
  {
    path: "blog/page/:pageID",
    component: BlogComponent,
    pathMatch: "full"
  }
];

@NgModule({
  declarations: [BlogComponent],
  imports: [
    IsLoadingModule,
    SinglePostModule,
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [BlogComponent]
})
export class BlogModule {}
