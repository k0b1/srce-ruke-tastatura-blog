import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PremotavanjeFrontpageComponent } from './premotavanje-frontpage.component';

describe('PremotavanjeFrontpageComponent', () => {
  let component: PremotavanjeFrontpageComponent;
  let fixture: ComponentFixture<PremotavanjeFrontpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PremotavanjeFrontpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PremotavanjeFrontpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
