import { Component, Output, OnInit, EventEmitter } from "@angular/core";
import { PostsService } from "src/app/services/posts.service";

@Component({
  selector: "app-premotavanje-frontpage-tabs",
  templateUrl: "./premotavanje-frontpage-tabs.component.html",
  styleUrls: ["./premotavanje-frontpage-tabs.component.scss"]
})
export class PremotavanjeFrontpageTabsComponent implements OnInit {
  post: any[];
  @Output() clickOnTab = new EventEmitter<any[]>();
  constructor(private posts: PostsService) {}

  ngOnInit() {
    // this.showOnlyMusicPosts();
  }

  showOnlyMusicPosts() {
    // get only music posts from database
    this.posts
      .mainGetRequest("premotavanje/muzika", "191", "12", "1", "")
      .subscribe(result => {
        
        // this.post = result;
        this.clickOnTab.emit(result);
      });
  }

  showOnlyBookPosts() {
    // get only book posts from database
    this.posts
      .mainGetRequest("premotavanje/knjige", "190", "12", "1", "")
      .subscribe(result => {
        
        // this.post = result;
        this.clickOnTab.emit(result);
      });
  }
}
