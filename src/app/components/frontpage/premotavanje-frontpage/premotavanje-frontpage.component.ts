import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { PostsService } from "../../../services/posts.service";

import { BbComponent } from "../../bb/bb.component";
import { PremotavanjeComponent } from "../../premotavanje/premotavanje.component";
import { BlogComponent } from "../../blog/blog.component";
import { PremotavanjeFrontpageTabsComponent } from "./premotavanje-frontpage-tabs/premotavanje-frontpage-tabs.component";
import { environment } from "./../../../../environments/environment";

@Component({
  selector: "app-premotavanje-frontpage",
  templateUrl: "./premotavanje-frontpage.component.html",
  styleUrls: ["./premotavanje-frontpage.component.scss"]
})
export class PremotavanjeFrontpageComponent implements OnInit {
  review: any[];
  isLoading: boolean = true;
  /* Review section variables*/
  music: any[];
  hide: boolean = false;
  opacity: string = "";
  selectedMusicPost: string = "";
  reviewImage: string = "";
  reviewTitle: string = "";
  reviewID: string = "";
  reviewExcerpt: string = "";
  reviewLink: string = "";
  linkToAsset: string = "";

  /* end of Review section variables */

  constructor(private postsService: PostsService) {}

  ngOnInit() {
    this.getReviewPosts("premotavanje", 12);
    this.linkToAsset = environment.linkToAsset;
  }

  getReviewPosts(post_type, numberOfPosts) {
    this.postsService
      .mainGetRequest(post_type, "190, 191", numberOfPosts, "1", "")
      .subscribe(result => {
        console.log("ALOOOO", result);
        this.isLoading = false;
        this.review = result;
        this.music = result;
      });
  }

  onHoverShowMusicData(post) {
    /* data to be sent to the right div container*/
    this.reviewImage = post.featured_image_src;
    this.reviewTitle = post.title.rendered;
    this.reviewID = post.id;
    this.reviewExcerpt = post.excerpt.rendered;
    this.reviewLink = post.link;
  }

  onLeaveMusicImage() {
    this.selectedMusicPost = "";
  }

  getTabClickData(event) {
    if (event[0].kategorije[0] == 191) {
      this.postsService
        .mainGetRequest("premotavanje/muzika", "191", "12", "1", "")
        .subscribe(result => {
          this.music = result;
        });
    } else if (event[0].kategorije[0] == 190) {
      this.postsService
        .mainGetRequest("premotavanje/knjige", "190", "12", "1", "")
        .subscribe(result => {
          this.music = result;
        });
    } else {
      this.getReviewPosts("premotavanje", 12);
    }
  }
}
