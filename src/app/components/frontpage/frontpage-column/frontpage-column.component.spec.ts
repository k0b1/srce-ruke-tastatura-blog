import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontpageColumnComponent } from './frontpage-column.component';

describe('FrontpageColumnComponent', () => {
  let component: FrontpageColumnComponent;
  let fixture: ComponentFixture<FrontpageColumnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontpageColumnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontpageColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
