import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-frontpage-column",
  templateUrl: "./frontpage-column.component.html",
  styleUrls: ["./frontpage-column.component.scss"]
})
export class FrontpageColumnComponent implements OnInit {
  @Input() postData: any[];
  @Input() post_type: string;
  constructor() {}

  ngOnInit() {}
}
