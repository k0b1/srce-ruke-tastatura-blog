import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { PostsService } from "../../services/posts.service";

import { BbComponent } from "./../bb/bb.component";
import { PremotavanjeComponent } from "./../premotavanje/premotavanje.component";
import { BlogComponent } from "./../blog/blog.component";
import { IsLoadingComponent } from "../shared/is-loading/is-loading.component";
import { environment } from './../../../environments/environment'; 
@Component({
  selector: "app-frontpage",
  templateUrl: "./frontpage.component.html",
  styleUrls: ["./frontpage.component.scss"]
})
export class FrontpageComponent implements OnInit {
  /* define wp rest api variables */
  posts;
  bb: any[];
  blogFront: any[];
  entryPost: any[];
  /* end of wp rest api variables */
  isLoading: boolean = true;
  linkToAsset: string;

  constructor(private postsService: PostsService) {}

  ngOnInit() {
    this.getIntroPostByTag();

      this.linkToAsset = environment.linkToAsset;
      
  }

  getIntroPostByTag() {
    this.postsService.getIntroPostByTag(83).subscribe(result => {
      this.isLoading = false;
      this.entryPost = result;
    });
  }

  // displayFrontpageContent(post_type, tagID, numberOfPosts, page_no, category) {
  //   this.postsService
  //     .mainGetRequest(post_type, "190, 191", numberOfPosts, '1', "") // 7 & 99 are categories for "Premotavanje" section
  //     .subscribe( result => {
  //       if ( post_type == "blog") this.blogFront = result;
         
  //     })
  // }
}
