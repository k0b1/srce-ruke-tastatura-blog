import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogFrontpageComponent } from './blog-frontpage.component';

describe('BlogFrontpageComponent', () => {
  let component: BlogFrontpageComponent;
  let fixture: ComponentFixture<BlogFrontpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogFrontpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogFrontpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
