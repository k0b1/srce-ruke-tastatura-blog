import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontpageRowsComponent } from './frontpage-rows.component';

describe('FrontpageRowsComponent', () => {
  let component: FrontpageRowsComponent;
  let fixture: ComponentFixture<FrontpageRowsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontpageRowsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontpageRowsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
