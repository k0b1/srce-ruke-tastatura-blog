import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-frontpage-rows",
  templateUrl: "./frontpage-rows.component.html",
  styleUrls: ["./frontpage-rows.component.scss"]
})
export class FrontpageRowsComponent implements OnInit {
  @Input() postData: any[];
  @Input() post_type: string;
  constructor() {}

  ngOnInit() {}
}
