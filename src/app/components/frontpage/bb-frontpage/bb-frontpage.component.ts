import { Component, OnInit } from "@angular/core";

import { SidebarComponent } from "../../../layouts/sidebar/sidebar.component";
import { PostsService } from "../../../services/posts.service";
import { Router } from "@angular/router";
import { ListPostsColumnComponent } from "../../shared/list-posts-column/list-posts-column.component";

@Component({
  selector: "app-bb-frontpage",
  templateUrl: "./bb-frontpage.component.html",
  styleUrls: ["./bb-frontpage.component.scss"]
})
export class BbFrontpageComponent implements OnInit {
  isLoading: boolean = true;
  bb: any[];
  isFrontPage: boolean;

  constructor(private postsService: PostsService, private router: Router) {}

  ngOnInit() {
    if (this.router.url == "/b1t0v11bajt0v1") {
      this.getB1BPosts("b1t0v11bajt0v1", "12");
      this.isFrontPage = false;
    } else if (this.router.url == "/") {
      this.getB1BPosts("b1t0v11bajt0v1", "3");
      this.isFrontPage = true;
    }
  }

  getB1BPosts(post_type: string, numberOfPosts: string) {
    this.postsService
      .mainGetRequest(post_type, "", numberOfPosts, "0", 1)
      .subscribe(result => {
        this.isLoading = false;
        this.bb = result;
      });
  }
}
