import { async, ComponentFixture, TestBed } from '@angular/core/testing';
 
import { BbFrontpageComponent } from './bb-frontpage.component';

describe('BbFrontpageComponent', () => {
  let component: BbFrontpageComponent;
  let fixture: ComponentFixture<BbFrontpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BbFrontpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BbFrontpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
