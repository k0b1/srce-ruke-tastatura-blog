import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-list-tags-frontpage',
  templateUrl: './list-tags.component.html',
  styleUrls: ['./list-tags.component.scss']
})
export class ListTagsFrontpageComponent implements OnInit {
  @Input() tags: any[];
  
  constructor() { }

  ngOnInit() {
  }

}
