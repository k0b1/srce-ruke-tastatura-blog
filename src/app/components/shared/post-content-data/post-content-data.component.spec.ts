import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostContentDataComponent } from './post-content-data.component';

describe('PostContentDataComponent', () => {
  let component: PostContentDataComponent;
  let fixture: ComponentFixture<PostContentDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostContentDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostContentDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
