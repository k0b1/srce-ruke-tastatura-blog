import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post-content-data',
  templateUrl: './post-content-data.component.html',
  styleUrls: ['./post-content-data.component.scss']
})
export class PostContentDataComponent implements OnInit {
  @Input() data;
  @Input() post_type;
  constructor() { }

  ngOnInit() {
  }

}
