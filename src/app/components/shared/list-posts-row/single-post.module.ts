import { NgModule } from "@angular/core";
import { ListPostsRowComponent } from "./list-posts-row.component";
import { PostMetaDataComponent } from "../post-meta-data/post-meta-data.component";
import { CommonModule } from "@angular/common";
import { PaginationComponent } from "../pagination/pagination.component";
import { RouterModule } from "@angular/router";
import { ListPostsColumnComponent } from "../list-posts-column/list-posts-column.component";
import { PostContentDataComponent } from "../post-content-data/post-content-data.component";
import { IsLoadingModule } from "../is-loading/is-loading.module";

@NgModule({
  declarations: [
    ListPostsRowComponent,
    ListPostsColumnComponent,
    PostMetaDataComponent,
    PaginationComponent,
    PostContentDataComponent
  ],
  imports: [CommonModule, RouterModule, IsLoadingModule],
  exports: [
    ListPostsRowComponent,
    ListPostsColumnComponent,
    PostMetaDataComponent,
    PostContentDataComponent,
    PaginationComponent
  ]
})
export class SinglePostModule {}
