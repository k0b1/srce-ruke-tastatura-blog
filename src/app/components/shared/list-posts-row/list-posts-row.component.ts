import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";

import { SidebarComponent } from "../../../layouts/sidebar/sidebar.component";
import { PostsService } from "../../../services/posts.service";
import { PaginationComponent } from "../../shared/pagination/pagination.component";

@Component({
  selector: "app-list-posts-row",
  templateUrl: "./list-posts-row.component.html",
  styleUrls: ["./list-posts-row.component.scss"]
})
export class ListPostsRowComponent implements OnInit {
  isLoading: boolean = true;
  httpResponse: any[];
  post_type: string = "premotavanje/muzika";
  currentPage: number = 1;
  metaImageCss: string = "music-image-front";
  id: number;

  constructor(
    private postsService: PostsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getMusicPosts("premotavanje/muzika", 191, 10, 1, ""); // 7 => music category id

    /*pagination blog load, listen to changes in url*/
    this.route.params.subscribe((params: Params) => {
      if (params["pageID"]) this.currentPage = params["pageID"];
      this.getMusicPosts("premotavanje/muzika", 191, 10, this.currentPage, ""); // 7 => music category id
    });
  }

  getMusicPosts(post_type, tagID, numberOfPosts, page_no, category) {
    this.postsService
      .mainGetRequest(post_type, tagID, numberOfPosts, page_no, category)
      .subscribe(result => {
        this.isLoading = false;
        this.httpResponse = result;
      });
  }
}
