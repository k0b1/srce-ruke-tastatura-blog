import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPostsRowComponent } from './list-posts-row.component';

describe('ListPostsRowComponent', () => {
  let component: ListPostsRowComponent;
  let fixture: ComponentFixture<ListPostsRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPostsRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPostsRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
