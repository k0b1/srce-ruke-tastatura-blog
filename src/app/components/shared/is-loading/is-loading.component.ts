import { Component, OnInit, Input } from "@angular/core";
import { environment } from "./../../../../environments/environment";

@Component({
  selector: "app-is-loading",
  templateUrl: "./is-loading.component.html",
  styleUrls: ["./is-loading.component.scss"]
})
export class IsLoadingComponent implements OnInit {
  @Input() isLoading: boolean;
  linkToAsset: string;

  constructor() {}

  ngOnInit() {
    this.linkToAsset = environment.linkToAsset;
      
      
  }
}
