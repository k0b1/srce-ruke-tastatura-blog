import { NgModule } from '@angular/core';
import { IsLoadingComponent } from './is-loading.component';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [IsLoadingComponent],
    imports:[CommonModule],
    exports: [IsLoadingComponent]
})
export class IsLoadingModule {

}
