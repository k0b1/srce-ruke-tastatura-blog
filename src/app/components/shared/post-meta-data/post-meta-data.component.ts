import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post-meta-data',
  templateUrl: './post-meta-data.component.html',
  styleUrls: ['./post-meta-data.component.scss']
})
export class PostMetaDataComponent implements OnInit {
  @Input() data:any[];
  @Input() type: string;
  constructor() { }

  ngOnInit() {
  }

}
