import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostMetaDataComponent } from './post-meta-data.component';

describe('PostMetaDataComponent', () => {
  let component: PostMetaDataComponent;
  let fixture: ComponentFixture<PostMetaDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostMetaDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostMetaDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
