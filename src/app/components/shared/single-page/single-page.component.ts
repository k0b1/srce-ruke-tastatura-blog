import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { PostsService } from "../../../services/posts.service";
import {
  DomSanitizer,
  SafeResourceUrl,
  SafeHtml,
  SafeUrl,
  Title,
  Meta,
} from "@angular/platform-browser";

@Component({
  selector: "app-single-page",
  templateUrl: "./single-page.component.html",
  styleUrls: ["./single-page.component.scss"],
})
export class SinglePageComponent implements OnInit {
  isLoading: boolean = true;
  post: any[];
  postTitle: string;
  postContent: string;
  postTags: [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private postsService: PostsService,
    public sanitizer: DomSanitizer,
    private titleService: Title,
    private metaTagService: Meta
  ) {}

  ngOnInit() {
    this.route.url.subscribe((getUrlPart) => {
      this.detectPageURL(getUrlPart[0].path);
    });
  }

  detectPageURL(url) {
    switch (url) {
      case "blog":
        this.getPost("blog", this.route.snapshot.params["post_slug"]);
        break;
      case "premotavanje":
        this.getPost("premotavanje", this.route.snapshot.params["post_slug"]);
        break;
      case "b1t0v11bajt0v1":
        this.getPost("b1t0v11bajt0v1", this.route.snapshot.params["post_slug"]);
        break;
      case "log":
        this.getIntroPostByTag(83);
    }
  }

  getPost(cpt, post_slug) {
    this.postsService.getPost(cpt, post_slug).subscribe((result) => {
      this.isLoading = false;

      this.post = result;
      console.log(result);
      this.titleService.setTitle(result[0].title.rendered);
      // check are metatags refreshed while navigating through different pages
      this.metaTagService.addTags(
        [
          {
            name: "keywords",
            content: result[0].tags_name
              .map((item) => {
                return item.name;
              })
              .join(", "),
          },
        ],
        true
      );
      this.postTitle = result[0].title.rendered;

      // this.postContent = this.sanitizer.bypassSecurityTrustHtml( result[0].content.rendered );
      this.postContent = result[0].title.rendered;
      this.postTags = result[0].tags_name;
    });
  }
  getIntroPostByTag(tagID: number) {
    this.postsService.getIntroPostByTag(tagID).subscribe((result) => {
      this.isLoading = false;
      this.post = result;
    });
  }
}
