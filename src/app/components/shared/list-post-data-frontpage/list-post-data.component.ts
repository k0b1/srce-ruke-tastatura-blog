import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-list-post-data-frontpage',
  templateUrl: './list-post-data.component.html',
  styleUrls: ['./list-post-data.component.scss']
})
export class ListPostDataFrontpageComponent implements OnInit {
  @Input() postMetaData;
  constructor() { }

  ngOnInit() {
  }
 
}
