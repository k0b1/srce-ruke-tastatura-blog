import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPostDataComponent } from './list-post-data.component';

describe('ListPostDataComponent', () => {
  let component: ListPostDataComponent;
  let fixture: ComponentFixture<ListPostDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPostDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPostDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
