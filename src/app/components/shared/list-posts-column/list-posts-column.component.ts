import {
  Component,
  OnInit,
  Input,
  SimpleChange,
  OnChanges
} from "@angular/core";
import { PostsService } from "src/app/services/posts.service";
import { ActivatedRoute, Router, Params } from "@angular/router";
//import { PaginationComponent } from "../../shared/pagination/pagination.component";

@Component({
  selector: "app-list-posts-column",
  templateUrl: "./list-posts-column.component.html",
  styleUrls: ["./list-posts-column.component.scss"]
})
export class ListPostsColumnComponent implements OnInit, OnChanges {
  isLoading: boolean = true;
  postsList: any[] = undefined;
  total_number_of_posts: number;
  currentPage: number = 1;
  post_type: string;
  tagID: string;
  tagName: string = "";
  isFrontPage: boolean;
  showContent: boolean = false;

  metaComponentCss: string = "featured-image blog-image img-thumbnail";

  postsPerPage: number = 10; //paginatiom, default

  @Input() searchTerm: string; // for search page
  @Input() isSearchPage: boolean = false;
  @Input() isTagPage: boolean = false;
  @Input() searchArea: string = "";
  @Input() searchRoute: string = "";
  @Input() content: any[];
  @Input() name: string = "";
  @Input() pageNumber: string;
  @Input() sectionTitle: string;

  constructor(
    private postsService: PostsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.tagName = this.name;
    this.isLoading = true;
    if (this.route.snapshot.url[2]) {
      this.tagID = this.route.snapshot.url[2].path;
    }
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    for (let propName in changes) {
      if (propName === "content") {
        this.postsList = changes[propName].currentValue;
        if (this.postsList) {
          this.post_type = this.postsList[0].type;
          this.showContent = true;
        } else {
          this.showContent = true;
        }
        this.isLoading = false;
      }
      if (propName === "name") {
        this.tagName = changes[propName].currentValue;
      }
    }
  }
}
