import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPostsColumnComponent } from './list-posts-column.component';

describe('ListPostsColumnComponent', () => {
  let component: ListPostsColumnComponent;
  let fixture: ComponentFixture<ListPostsColumnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPostsColumnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPostsColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
