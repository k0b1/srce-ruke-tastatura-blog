import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchBySectionComponent } from './search-by-section.component';

describe('SearchBySectionComponent', () => {
  let component: SearchBySectionComponent;
  let fixture: ComponentFixture<SearchBySectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchBySectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBySectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
