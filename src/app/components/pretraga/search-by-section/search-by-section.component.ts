import { Component, OnInit } from "@angular/core";
import { PostsService } from "src/app/services/posts.service";
import { ActivatedRoute, Router } from "@angular/router";

import { ListPostsColumnComponent } from "./../../shared/list-posts-column/list-posts-column.component";

@Component({
  selector: "app-search-by-section",
  templateUrl: "./search-by-section.component.html",
  styleUrls: ["./search-by-section.component.scss"]
})
export class SearchBySectionComponent implements OnInit {
  isFrontPage: boolean = false;
  isLoading: boolean = true;
  searchItem: string;
  postType: string;
  searchRoute: string;
  databaseContent: any[];
  tagID: number = 1;
  page_number: string = "1";

  constructor(
    private postService: PostsService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.searchItem = this.route.snapshot.params["searchItem"];
    this.postType = this.route.snapshot.params["post_type"];

    if (this.route.snapshot.url[1].path === "pretraga") {
      this.page_number = this.route.snapshot.url[3].path;
    }

    if (this.route.snapshot.url[1].path === "tagovi") {
      this.searchItem = this.route.snapshot.url[3].path;
    }
    this.searchRoute =
      this.route.snapshot.url[0] + "/" + this.route.snapshot.url[1];

    this.route.params.subscribe(result => {
      if (this.route.snapshot.url[1].path == "pretraga") {
        this.getListBySearchItem(
          result.post_type + "/pretraga",
          result.searchItem,
          result.pageID
        );
      }
      if (this.route.snapshot.url[1].path == "tagovi") {
        this.getListBySearchItem(
          result.post_type + "/tagovi",
          result.tag_id,
          result.pageID
        );
      }
    });

    this.getListBySearchItem(
      this.searchRoute,
      this.searchItem,
      this.page_number
    );
  }

  getListBySearchItem(postType, searchItem, page_number) {
    return this.postService
      .mainGetRequest(postType, searchItem, "10", page_number, "")
      .subscribe(result => {
        this.databaseContent = result;
        this.isLoading = false;
      });
  }
}
