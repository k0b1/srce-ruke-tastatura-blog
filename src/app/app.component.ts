import { Component, OnInit } from "@angular/core";
import { Title, Meta } from "@angular/platform-browser";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  title = "srce-ruke-tastatura";
  token = null;

  constructor(private titleService: Title, private metaTagService: Meta) {}

  ngOnInit() {
    this.titleService.setTitle("Srce, ruke, tastatura");
    this.metaTagService.addTags([
      {
        name: "keywords",
        content:
          "Javascript, punk, pank, counterculture, kontrakultura, hacking, anarchism, programiranje, programming, recenzije, reviews, learning, učenje, internet",
      },
      { name: "robots", content: "index, follow" },
      { name: "author", content: "k0b1" },
      { charset: "UTF-8" },
    ]);
  }
}
