export const environment = {
    production: true,
    baseUrl: 'https://srce-ruke-tastatura.rocks',
    linkToAsset: 'wp-content/themes/srce-ruke-tastatura/dist/'
};
